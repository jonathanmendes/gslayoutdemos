(function() {
  'use strict';

  angular.module('application', [
    'ui.router',
    'ngAnimate',

    //foundation
    'foundation',
    'foundation.dynamicRouting',
    'foundation.dynamicRouting.animations',

    'controller.verticalLayout',

    // 'partners',
    'materials',
    'signups',
  ])
    .config(config)
    .run(run)
  ;

  config.$inject = ['$urlRouterProvider', '$locationProvider'];

  function config($urlProvider, $locationProvider) {
    $urlProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled:false,
      requireBase: false
    });

    $locationProvider.hashPrefix('!');
  }

  function run() {
    FastClick.attach(document.body);
  }

})();

(function(window, document, angular, undefined){
    'use strict';

    angular.module('controller.verticalLayout', [])

    .controller('verticalLayout', [ '$scope', '$rootScope', '$timeout', 'FoundationApi', function($scope, $rootScope, $timeout, FoundationApi) {

        $scope.activeAction = 1;
        $scope.partnerTab = 1;
        $scope.trackingTab = 1;
        $scope.showSignups = true;

        $scope.partners = [
            {
                'firstName': 'Bryn',
                'lastName': 'Jones',
                'email': 'bryn@growsumo.com',
                'unreadMessages': '3',
                'transactionsLength': '1',
                'lastTransaction': 'May 2nd, 2015',
                'transactions': [
                    {
                        'name': 'Jonathan Mendes',
                        'email': 'jonathan.a.mendes@gmail.com',
                        'created_at': 4809745893458
                    },
                    {
                        'name': 'Luke Swanek',
                        'email': 'luke@growsumo.com',
                        'created_at': 480974589345812
                    },
                    {
                        'name': 'Neil Chudleigh',
                        'email': 'neil@growsumo.com',
                        'created_at': 480974589345833
                    }
                ],
                'notes': [
                    {
                        'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                        'created_at': 987345987345789
                    },
                    {
                        'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                        'created_at': 987345987345789
                    },
                    {
                        'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                        'created_at': 987345987345789
                    },
                ]
            },
            {
                'firstName': 'Luke',
                'lastName': 'Swanek',
                'email': 'luke@growsumo.com',
                'unreadMessages': '1',
                'transactionsLength': '4',
                'lastTransaction': 'May 1st, 2015',
                'transactions': [
                    {
                        'name': 'Jonathan Mendes',
                        'email': 'jonathan.a.mendes@gmail.com',
                        'created_at': 4809745893458
                    },
                    {
                        'name': 'Bryn Jones',
                        'email': 'bryn@growsumo.com',
                        'created_at': 480974589345812
                    },
                    {
                        'name': 'Neil Chudleigh',
                        'email': 'neil@growsumo.com',
                        'created_at': 480974589345833
                    }
                ],
                'notes': [
                    {
                        'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                        'created_at': 987345987345789
                    },
                    {
                        'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                        'created_at': 987345987345789
                    },
                    {
                        'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                        'created_at': 987345987345789
                    },
                ]
            },
            {
                'firstName': 'Neil',
                'lastName': 'Chudleigh',
                'email': 'neil@growsumo.com',
                'unreadMessages': '5',
                'transactionsLength': '3',
                'lastTransaction': 'February 22nd, 2015',
                'transactions': [
                    {
                        'name': 'Jonathan Mendes',
                        'email': 'jonathan.a.mendes@gmail.com',
                        'created_at': 4809745893458
                    },
                    {
                        'name': 'Bryn Jones',
                        'email': 'bryn@growsumo.com',
                        'created_at': 480974589345812
                    },
                    {
                        'name': 'Luke Swanek',
                        'email': 'luke@growsumo.com',
                        'created_at': 480974589345833
                    }
                ],
                'notes': [
                    {
                        'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                        'created_at': 987345987345789
                    },
                    {
                        'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                        'created_at': 987345987345789
                    },
                    {
                        'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                        'created_at': 987345987345789
                    },
                ]
            },
            {
                'firstName': 'Jonathan',
                'lastName': 'Mendes',
                'email': 'jon@growsumo.com',
                'unreadMessages': '0',
                'transactionsLength': '2',
                'lastTransaction': 'December 1st, 2014',
                'transactions': [
                    {
                        'name': 'Jonathan Mendes',
                        'email': 'jonathan.a.mendes@gmail.com',
                        'created_at': 4809745893458,
                    },
                    {
                        'name': 'Bryn Jones',
                        'email': 'bryn@growsumo.com',
                        'created_at': 480974589345812
                    },
                    {
                        'name': 'Luke Swanek',
                        'email': 'luke@growsumo.com',
                        'created_at': 480974589345833
                    }
                ],
                'notes': [
                    {
                        'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                        'created_at': 987345987345789
                    },
                    {
                        'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                        'created_at': 987345987345789
                    },
                    {
                        'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                        'created_at': 987345987345789
                    },
                ]
            }
        ];

        $scope.companies = [
            {
                'approved': true,
                'competitors': "Time Matters↵Excel↵Pen and Paper",
                'created_at': 1425595963018,
                'creator_id': 23,
                'customers': "Small to Medium size law firms↵1-50 attorneys",
                'description': "Clio is the lead cloud-based practice management solution for lawyers. Managed everything from intake to invoicing, all in one program.",
                'extra': "",
                'features': "Case Management↵Time Tracking↵Billing↵Reporting↵Document Create and Management↵Contacts and Calendars↵And more!",
                'id': 45,
                'img': "companyLogo/ApfSZMfLHNBqdN4OzpOe.jpeg",
                'name': "Clio",
                'phone': "",
                'slogan': "Helping Lawyers save time and money!",
                'slug': "clio",
                'updated_at': 1425665968192,
                'website': "www.clio.com"
            },
            {
                'approved': true,
                'competitors': "Cloud accounting and invoicing software",
                'created_at': 1429717928004,
                'creator_id': 81,
                'customers': "With 2.5 million users around the world, Wave is the fastest-growing financial software for small businesses on the planet.",
                'description': "All the tools you need for managing your small business finances, in one place. Easy & professional, for confidence and peace of mind.",
                'extra': "Wave grows with your business. When you're ready, add optional payroll with direct deposit, and credit card processing.",
                'features': "• unlimited invoices• unlimited expense tracking• real accounting, made simple; accountant approved• integrated personal finance",
                'id': 71,
                'img': "companyLogo/6ZzLxpJMk1fsirQqVDoH.png",
                'name': "Wave",
                'phone': "",
                'slogan': "100% free accounting, invoicing & more for small businesses!",
                'slug': "wave",
                'updated_at': 1429717928004,
                'website': "waveapps.com"
            }
        ];

        $scope.messages = [
            {
                'body': 'Just a lil shout out to ma boy neil',
                'created_at': 897324879243,
                'fromCompany': false
            },
            {
                'body': 'hi.',
                'created_at': 897324879243,
                'fromCompany': true
            },
        ];

        $scope.openMessageBox = function(partner) {
            FoundationApi.publish('partnerPanel', 'open');
            $scope.selectedPartner = partner;
            $scope.showSignups = true;
        };


    }])
    ;}(window, document, angular, undefined));

(function( document, angular, undefined) {
    'use strict';

    angular.module('materials', [])

    .directive('materials',['$timeout', function($timeout){
        return{
            restrict: 'E',
            templateUrl: '/templates/directives/materials/materials.html',
            scope:{
            },
            link: function(scope, elem, attrs) {

                scope.uploading = false;
                scope.newMaterials = [];


                scope.reset = function() {
                    scope.errors = {
                        url: ''
                    };
                    scope.valid = false;
                    scope.errorMessage = '';
                    scope.uploadSuccess = false;
                };
                scope.reset();


            }
        }
    }]);

    ;}( document, angular, undefined));

(function( document, angular, undefined) {
    'use strict';

    angular.module('partners', [])

    .directive('partners',['$timeout', function($timeout){
        return{
            restrict: 'E',
            templateUrl: '/templates/directives/partners/partners.html',
            scope:{
                partnerTab: '=',
                showSignups: '='
            },
            link: function(scope, elem, attrs) {

                scope.showSignups = true;

                scope.partners = [
                    {
                        'firstName': 'Bryn',
                        'lastName': 'Jones',
                        'email': 'bryn@growsumo.com',
                        'unreadMessages': '3',
                        'transactionsLength': '1',
                        'lastTransaction': 'May 2nd, 2015',
                        'transactions': [
                            {
                                'name': 'Jonathan Mendes',
                                'email': 'jonathan.a.mendes@gmail.com',
                                'created_at': 4809745893458
                            },
                            {
                                'name': 'Luke Swanek',
                                'email': 'luke@growsumo.com',
                                'created_at': 480974589345812
                            },
                            {
                                'name': 'Neil Chudleigh',
                                'email': 'neil@growsumo.com',
                                'created_at': 480974589345833
                            }
                        ],
                        'notes': [
                            {
                                'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                                'created_at': 987345987345789
                            },
                        ]
                    },
                    {
                        'firstName': 'Luke',
                        'lastName': 'Swanek',
                        'email': 'luke@growsumo.com',
                        'unreadMessages': '1',
                        'transactionsLength': '4',
                        'lastTransaction': 'May 1st, 2015',
                        'transactions': [
                            {
                                'name': 'Jonathan Mendes',
                                'email': 'jonathan.a.mendes@gmail.com',
                                'created_at': 4809745893458
                            },
                            {
                                'name': 'Bryn Jones',
                                'email': 'bryn@growsumo.com',
                                'created_at': 480974589345812
                            },
                            {
                                'name': 'Neil Chudleigh',
                                'email': 'neil@growsumo.com',
                                'created_at': 480974589345833
                            }
                        ],
                        'notes': [
                            {
                                'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                                'created_at': 987345987345789
                            },
                        ]
                    },
                    {
                        'firstName': 'Neil',
                        'lastName': 'Chudleigh',
                        'email': 'neil@growsumo.com',
                        'unreadMessages': '5',
                        'transactionsLength': '3',
                        'lastTransaction': 'February 22nd, 2015',
                        'transactions': [
                            {
                                'name': 'Jonathan Mendes',
                                'email': 'jonathan.a.mendes@gmail.com',
                                'created_at': 4809745893458
                            },
                            {
                                'name': 'Bryn Jones',
                                'email': 'bryn@growsumo.com',
                                'created_at': 480974589345812
                            },
                            {
                                'name': 'Luke Swanek',
                                'email': 'luke@growsumo.com',
                                'created_at': 480974589345833
                            }
                        ],
                        'notes': [
                            {
                                'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                                'created_at': 987345987345789
                            },
                        ]
                    },
                    {
                        'firstName': 'Jonathan',
                        'lastName': 'Mendes',
                        'email': 'jon@growsumo.com',
                        'unreadMessages': '0',
                        'transactionsLength': '2',
                        'lastTransaction': 'December 1st, 2014',
                        'transactions': [
                            {
                                'name': 'Jonathan Mendes',
                                'email': 'jonathan.a.mendes@gmail.com',
                                'created_at': 4809745893458,
                            },
                            {
                                'name': 'Bryn Jones',
                                'email': 'bryn@growsumo.com',
                                'created_at': 480974589345812
                            },
                            {
                                'name': 'Luke Swanek',
                                'email': 'luke@growsumo.com',
                                'created_at': 480974589345833
                            }
                        ],
                        'notes': [
                            {
                                'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                                'created_at': 987345987345789
                            },
                        ]
                    }
                ];

                scope.messages = [
                    {
                        'body': 'Just a lil shout out to ma boy neil',
                        'created_at': 897324879243,
                        'fromCompany': false
                    },
                    {
                        'body': 'hi.',
                        'created_at': 897324879243,
                        'fromCompany': true
                    },
                ];

                rootScope.openMessageBox = function(partner) {
                    FoundationApi.publish('partnerPanel', 'open');
                    scope.selectedPartner = partner;
                    scope.showSignups = true;
                };


            }
        }
    }]);

    ;}( document, angular, undefined));

(function( document, angular, undefined) {
    'use strict';

    angular.module('signups', [])

    .directive('signups',['$timeout', function($timeout){
        return{
            restrict: 'E',
            templateUrl: '/templates/directives/signups/signups.html',
            scope:{
            },
            link: function(scope, elem, attrs) {

                scope.signups = [
                    {
                        'email': 'jonathan.a.mendes@gmail.com',
                        'creator': {
                            'firstName': 'Jonathan',
                            'lastName': 'Mendes'
                        },
                        'date': 879345789345,
                        'approved': false,
                        'declined': false,
                    },
                    {
                        'email': 'jonathan.a.mendes@gmail.com',
                        'creator': {
                            'firstName': 'Jonathan',
                            'lastName': 'Mendes'
                        },
                        'date': 879345789345,
                        'approved': true,
                        'declined': false,
                    },
                    {
                        'email': 'jonathan.a.mendes@gmail.com',
                        'creator': {
                            'firstName': 'Jonathan',
                            'lastName': 'Mendes'
                        },
                        'date': 879345789345,
                        'approved': false,
                        'declined': true,
                    },
                ];

            }
        }
    }]);

    ;}( document, angular, undefined));
