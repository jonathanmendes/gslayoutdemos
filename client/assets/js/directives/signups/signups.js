(function( document, angular, undefined) {
    'use strict';

    angular.module('signups', [])

    .directive('signups',['$timeout', function($timeout){
        return{
            restrict: 'E',
            templateUrl: '/templates/directives/signups/signups.html',
            scope:{
            },
            link: function(scope, elem, attrs) {

                scope.signups = [
                    {
                        'email': 'jonathan.a.mendes@gmail.com',
                        'creator': {
                            'firstName': 'Jonathan',
                            'lastName': 'Mendes'
                        },
                        'date': 879345789345,
                        'approved': false,
                        'declined': false,
                    },
                    {
                        'email': 'jonathan.a.mendes@gmail.com',
                        'creator': {
                            'firstName': 'Jonathan',
                            'lastName': 'Mendes'
                        },
                        'date': 879345789345,
                        'approved': true,
                        'declined': false,
                    },
                    {
                        'email': 'jonathan.a.mendes@gmail.com',
                        'creator': {
                            'firstName': 'Jonathan',
                            'lastName': 'Mendes'
                        },
                        'date': 879345789345,
                        'approved': false,
                        'declined': true,
                    },
                ];

            }
        }
    }]);

    ;}( document, angular, undefined));
