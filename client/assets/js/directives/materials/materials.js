(function( document, angular, undefined) {
    'use strict';

    angular.module('materials', [])

    .directive('materials',['$timeout', function($timeout){
        return{
            restrict: 'E',
            templateUrl: '/templates/directives/materials/materials.html',
            scope:{
            },
            link: function(scope, elem, attrs) {

                scope.uploading = false;
                scope.newMaterials = [];


                scope.reset = function() {
                    scope.errors = {
                        url: ''
                    };
                    scope.valid = false;
                    scope.errorMessage = '';
                    scope.uploadSuccess = false;
                };
                scope.reset();


            }
        }
    }]);

    ;}( document, angular, undefined));
