(function( document, angular, undefined) {
    'use strict';

    angular.module('partners', [])

    .directive('partners',['$timeout', function($timeout){
        return{
            restrict: 'E',
            templateUrl: '/templates/directives/partners/partners.html',
            scope:{
                partnerTab: '=',
                showSignups: '='
            },
            link: function(scope, elem, attrs) {

                scope.showSignups = true;

                scope.partners = [
                    {
                        'firstName': 'Bryn',
                        'lastName': 'Jones',
                        'email': 'bryn@growsumo.com',
                        'unreadMessages': '3',
                        'transactionsLength': '1',
                        'lastTransaction': 'May 2nd, 2015',
                        'transactions': [
                            {
                                'name': 'Jonathan Mendes',
                                'email': 'jonathan.a.mendes@gmail.com',
                                'created_at': 4809745893458
                            },
                            {
                                'name': 'Luke Swanek',
                                'email': 'luke@growsumo.com',
                                'created_at': 480974589345812
                            },
                            {
                                'name': 'Neil Chudleigh',
                                'email': 'neil@growsumo.com',
                                'created_at': 480974589345833
                            }
                        ],
                        'notes': [
                            {
                                'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                                'created_at': 987345987345789
                            },
                        ]
                    },
                    {
                        'firstName': 'Luke',
                        'lastName': 'Swanek',
                        'email': 'luke@growsumo.com',
                        'unreadMessages': '1',
                        'transactionsLength': '4',
                        'lastTransaction': 'May 1st, 2015',
                        'transactions': [
                            {
                                'name': 'Jonathan Mendes',
                                'email': 'jonathan.a.mendes@gmail.com',
                                'created_at': 4809745893458
                            },
                            {
                                'name': 'Bryn Jones',
                                'email': 'bryn@growsumo.com',
                                'created_at': 480974589345812
                            },
                            {
                                'name': 'Neil Chudleigh',
                                'email': 'neil@growsumo.com',
                                'created_at': 480974589345833
                            }
                        ],
                        'notes': [
                            {
                                'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                                'created_at': 987345987345789
                            },
                        ]
                    },
                    {
                        'firstName': 'Neil',
                        'lastName': 'Chudleigh',
                        'email': 'neil@growsumo.com',
                        'unreadMessages': '5',
                        'transactionsLength': '3',
                        'lastTransaction': 'February 22nd, 2015',
                        'transactions': [
                            {
                                'name': 'Jonathan Mendes',
                                'email': 'jonathan.a.mendes@gmail.com',
                                'created_at': 4809745893458
                            },
                            {
                                'name': 'Bryn Jones',
                                'email': 'bryn@growsumo.com',
                                'created_at': 480974589345812
                            },
                            {
                                'name': 'Luke Swanek',
                                'email': 'luke@growsumo.com',
                                'created_at': 480974589345833
                            }
                        ],
                        'notes': [
                            {
                                'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                                'created_at': 987345987345789
                            },
                        ]
                    },
                    {
                        'firstName': 'Jonathan',
                        'lastName': 'Mendes',
                        'email': 'jon@growsumo.com',
                        'unreadMessages': '0',
                        'transactionsLength': '2',
                        'lastTransaction': 'December 1st, 2014',
                        'transactions': [
                            {
                                'name': 'Jonathan Mendes',
                                'email': 'jonathan.a.mendes@gmail.com',
                                'created_at': 4809745893458,
                            },
                            {
                                'name': 'Bryn Jones',
                                'email': 'bryn@growsumo.com',
                                'created_at': 480974589345812
                            },
                            {
                                'name': 'Luke Swanek',
                                'email': 'luke@growsumo.com',
                                'created_at': 480974589345833
                            }
                        ],
                        'notes': [
                            {
                                'content': 'No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.',
                                'created_at': 987345987345789
                            },
                            {
                                'content': 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
                                'created_at': 987345987345789
                            },
                        ]
                    }
                ];

                scope.messages = [
                    {
                        'body': 'Just a lil shout out to ma boy neil',
                        'created_at': 897324879243,
                        'fromCompany': false
                    },
                    {
                        'body': 'hi.',
                        'created_at': 897324879243,
                        'fromCompany': true
                    },
                ];

                rootScope.openMessageBox = function(partner) {
                    FoundationApi.publish('partnerPanel', 'open');
                    scope.selectedPartner = partner;
                    scope.showSignups = true;
                };


            }
        }
    }]);

    ;}( document, angular, undefined));
